<!DOCTYPE html>
<html>
  <head>
    <title>Google Sheets API Quickstart</title>
    <meta charset="utf-8" />
    <script src="jquery.min.js" ></script>
    <script>
        function processData(allText) {
            var record_num = 11;  // or however many elements there are in each row
            var allTextLines = allText.split(/\r\n|\n/);
            var city = [];
            var description = []
            var lines = [];
            for(var i =0 ; i<allTextLines.length;i++)
                {
                    var entries = allTextLines[i].split(',');
                    var cty = new String(entries[4]);
                    var country = new String(entries[3])
                    var txt =  cty.concat(' ' ,country) ;
                    if($.inArray(txt,city)==-1)
                        {
                            city.push(txt);
                            var name= new String(entries[1]);
                            var ratinglevel = new String(entries[8]);
                            var desc = name.concat(': ' , ratinglevel, "\n");
                            description.push(desc);
                        }
                    else{
                        var idx = $.inArray(txt,city)
                        var desc1 = new String(description[idx])
                        var name= new String(entries[1]);
                        var ratinglevel = new String(entries[8]);
                        var desc =desc1.concat(name, ': ' , ratinglevel, "\n");
                        description[idx] = desc;
                    }

                    
                        var tarr = [];
                        for (var j=0; j<record_num; j++) {
                            tarr.push(entries[j]);
                        }
                        lines.push(tarr);
                        var txt = $('#data').text() + "<br>";
                       
                        //alert(lines);
                       // $('#data').text(lines);
                    
                }
             //$('#data').text(lines.join("\n"));
            $('#city').text(city.join("\n"));
            $('#desc').text(description.join("\n"));
            
        }
    </script>
  </head>
  <body>
    <pre id='data'></pre>
      <pre id='city'></pre>
      <pre id='desc'></pre>
    <script>
        $(document).ready(function() {
        $.ajax({
            type: "GET",
            url: "EDSCertifiedProjects1.csv",
            dataType: "text",
            success: function(data) {processData(data);}
         });
    });
      </script>
  </body>
</html>