# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary: 
This is for showing EDS projects on Google map that in future get added to EDS Website. 
This is a small 1 or 2 web pages for showing and managing EDS completed projects.
For now this only include a web page that show some filter options along with map.
The EDS project detail is saved in CSV file.
* Version #1


### How do I get set up? ###

* To design the webpages only HTML and Javascript is used, so this can be run on any web browser.
* Front-end: HTML + JAVAScript
* Database: CSV file


### points to Note or future implementations ###
* This is using CSV file for storing EDS projects details. There is a problem in showing commas(,) in the project name. We can try showing same info with excel file
* Version 1: pending -Finalize info to be displayed on screen
* Version 2: 	-create a page for adding completed project list to csv/excel file
			-Use excel file in place of CSV file
			

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Nikunj Shukla, Application Developer and Trainer, EDS
* Contact at nikunj@edsglobal.com